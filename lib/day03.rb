require "pry-byebug"

class Day03
  LETTERS = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
    "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B",
    "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q",
    "R", "S", "T", "U", "V", "W", "X", "Y", "Z"].freeze

  class << self
    def first_puzzle(input_file = "./data/day03-part01.txt")
      rucksacks = read_data(input_file)
      rucksacks.map do |rucksack|
        items_by_compartment = rucksack.length / 2
        first_compartment = rucksack[0...items_by_compartment].chars
        second_compartment = rucksack[items_by_compartment..].chars
        LETTERS.index((first_compartment & second_compartment).first) + 1
      end.sum
    end

    def second_puzzle(input_file = "./data/day03-part01.txt")
      all_rucksacks = read_data(input_file)
      all_rucksacks.each_slice(3).map do |rucksacks|
        badge = (rucksacks[0].chars & rucksacks[1].chars & rucksacks[2].chars).first
        LETTERS.index(badge) + 1
      end.sum
    end

    def read_data(input_file)
      IO.read(input_file).split("\n")
    end
  end
end

unless ENV["TEST_ENV"]
  puts "First puzzle: #{Day03.first_puzzle}"
  puts "Second puzzle: #{Day03.second_puzzle}"
end
