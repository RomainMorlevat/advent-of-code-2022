require "pry-byebug"

class Day02
  ALL_HANDS = {
    "A X" => 4,
    "A Y" => 8,
    "A Z" => 3,
    "B X" => 1,
    "B Y" => 5,
    "B Z" => 9,
    "C X" => 7,
    "C Y" => 2,
    "C Z" => 6
  }.freeze

  ALL_HANDS_SECOND_PART = {
    "A X" => 3,
    "A Y" => 4,
    "A Z" => 8,
    "B X" => 1,
    "B Y" => 5,
    "B Z" => 9,
    "C X" => 2,
    "C Y" => 6,
    "C Z" => 7
  }.freeze

  SHAPE_SCORE = {
    "A" => 1,
    "B" => 2,
    "C" => 3,
    "X" => 1,
    "Y" => 2,
    "Z" => 3
  }.freeze

  class << self
    def first_puzzle(input_file)
      rounds = IO.read(input_file).split("\n")
      rounds.map { |hands| ALL_HANDS[hands] }.sum
    end

    def second_puzzle(input_file)
      rounds = IO.read(input_file).split("\n")
      rounds.map { |hands| ALL_HANDS_SECOND_PART[hands] }.sum
    end

    # I wasn't satisfyied with my first commit that were not working outside
    # tests and I knew that there were a sort of mathematical relation to get
    # if win/draw/loose
    def first_puzzle_2(input_file)
      rounds = IO.read(input_file).split("\n")
      rounds.map do |hands|
        first_shape = SHAPE_SCORE[hands[0]]
        second_shape = SHAPE_SCORE[hands[-1]]

        case (first_shape - second_shape) % 3
        when 0
          second_shape + 3
        when 2
          second_shape + 6
        else
          second_shape
        end
      end.sum
    end
  end
end

unless ENV["TEST_ENV"]
  puts "First puzzle: #{Day02.first_puzzle("./data/day02-part01.txt")}"
  puts "Second puzzle: #{Day02.second_puzzle("./data/day02-part01.txt")}"
  puts "Second try: #{Day02.first_puzzle_2("./data/day02-part01.txt")}"
end
