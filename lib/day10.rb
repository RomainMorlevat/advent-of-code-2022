require "pry-byebug"

class Day10
  class << self
    def first_puzzle(input_file = "./data/day10-part01.txt")
      x = 1
      signal_strength = []
      prepare_instructions(input_file).each_with_index do |amount, index|
        cycle = index + 1
        if (cycle - 20) % 40 == 0
          signal_strength << x * cycle
        end
        x += amount
      end
      signal_strength.sum
    end

    def second_puzzle(input_file = "./data/day10-part01.txt")
      crt = []
      x = 1
      prepare_instructions(input_file).each_slice(40).map do |line|
        line.each_with_index do |amount, index|
          char = x.between?(index - 1, index + 1) ? "#" : "."
          crt.push(char)
          x += amount
        end
      end

      crt.each_slice(40).map { |line| line.join }
    end

    def prepare_instructions(input_file)
      IO.read(input_file).split("\n").map do |line|
        instruction, amount = line.split(" ")
        if instruction == "noop"
          [0]
        else
          [0, amount.to_i]
        end
      end.flatten
    end
  end
end

unless ENV["TEST_ENV"]
  puts "First puzzle: #{Day10.first_puzzle}"
  puts "Second puzzle:"
  Day10.second_puzzle.each { |line| puts line }
end
