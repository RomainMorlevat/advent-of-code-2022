require "pry-byebug"

class Day06
  def initialize(input_file = "./data/day06-part01.txt")
    @input = IO.read(input_file)
  end

  def first_puzzle
    first_marker_after(4)
  end

  def second_puzzle
    first_marker_after(14)
  end

  def first_marker_after(distinct_count)
    @input.chars.each_with_index do |_l, i|
      if @input[i...i + distinct_count].chars.uniq.count == distinct_count
        return i + distinct_count
      end
    end
  end
end

unless ENV["TEST_ENV"]
  puts "First puzzle: #{Day06.new.first_puzzle}"
  puts "Second puzzle: #{Day06.new.second_puzzle}"
end
