require "pry-byebug"

class Day07
  class << self
    def build_tree(input_file = "./data/day07-part01.txt")
      input = IO.read(input_file).split("\n")
      tree = [0]
      pwd = [0]
      # ohh, this is a really 💩 code
      input.each do |line|
        if line[-1] == "/"
          next
        elsif line[0] == "$"
          if line.match?(/cd \w/)
            if pwd.length == 1
              tree.push([0])
            else
              tree.dig(*pwd[0...-1]).push([0])
            end
            pwd[-1] += 1
            pwd.push(0)
          elsif line.match?(/cd ../)
            pwd.pop
          end
        else
          content = line.gsub(/[^0-9]/, "").to_i
          next if content.zero?
          if pwd.length == 1
            tree[*pwd] += content
          else
            pwd.length.times do |n|
              if n == 0
                tree[0] += content
              else
                tree.dig(*pwd[0...n])[0] += content
              end
            end
          end
        end
      end
      tree
    end

    def first_puzzle(tree = build_tree)
      tree.flatten.select { |size| size <= 100_000 }.sum
    end

    def second_puzzle(tree = build_tree)
      tree = tree.flatten
      remaining_size = 70_000_000 - tree.max
      size_to_free = 30_000_000 - remaining_size
      tree.select { |dir_size| dir_size >= size_to_free }.min
    end
  end
end

unless ENV["TEST_ENV"]
  puts "First puzzle: #{Day07.first_puzzle}"
  puts "Second puzzle: #{Day07.second_puzzle}"
end
