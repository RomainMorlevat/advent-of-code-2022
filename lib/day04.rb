require "pry-byebug"

class Day04
  class << self
    def first_puzzle(input_file = "./data/day04-part01.txt")
      all_pairs = read_data(input_file)
      all_pairs.count do |pairs|
        pair_1_sections, pair_2_sections = extract_pairs_sections(pairs)
        max_length = [pair_1_sections.length, pair_2_sections.length].max
        pair_1_sections.union(pair_2_sections).length == max_length
      end
    end

    def second_puzzle(input_file = "./data/day04-part01.txt")
      all_pairs = read_data(input_file)
      all_pairs.count do |pairs|
        pair_1_sections, pair_2_sections = extract_pairs_sections(pairs)
        !pair_1_sections.intersection(pair_2_sections).empty?
      end
    end

    def extract_pairs_sections(pairs)
      pairs.split(",").map do |sections|
        section_start, section_end = sections.split("-")
        (section_start.to_i..section_end.to_i).to_a
      end
    end

    def read_data(input_file)
      IO.read(input_file).split("\n")
    end
  end
end

unless ENV["TEST_ENV"]
  puts "First puzzle: #{Day04.first_puzzle}"
  puts "Second puzzle: #{Day04.second_puzzle}"
end
