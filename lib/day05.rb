require "pry-byebug"

class Day05
  class << self
    def build_columns(columns)
      cols = columns.last.split(" ").map { |col_number| [col_number, []] }.to_h
      columns[0...-1].each do |col|
        temp = col.chars.each_slice(4).map(&:join).map { |c| c.gsub(/\[|\]/, "") }.map(&:strip)
        temp.each_with_index do |letter, index|
          next if letter.empty?
          cols[(index + 1).to_s].unshift(letter)
        end
      end
      cols
    end

    def build_moves(moves)
      moves.map do |move|
        move.split(/\D/).reject(&:empty?)
      end
    end

    def first_puzzle(input_file = "./data/day05-part01.txt")
      input = IO.read(input_file).split("\n")
      split_input = split_input(input)
      columns = build_columns(split_input.first)
      moves = build_moves(split_input.last)
      moves.each do |move|
        quantity, from, to = move
        letters = columns[from].pop(quantity.to_i).reverse
        letters.each { |letter| columns[to].push(letter) }
      end
      columns.values.map(&:last).join
    end

    def second_puzzle(input_file = "./data/day05-part01.txt")
      input = IO.read(input_file).split("\n")
      split_input = split_input(input)
      columns = build_columns(split_input.first)
      moves = build_moves(split_input.last)
      moves.each do |move|
        quantity, from, to = move
        letters = columns[from].pop(quantity.to_i)
        letters.each { |letter| columns[to].push(letter) }
      end
      columns.values.map(&:last).join
    end

    def split_input(input)
      blank_line_index = input.find_index("")
      [input[0...blank_line_index], input[(blank_line_index + 1)..]]
    end
  end
end

unless ENV["TEST_ENV"]
  puts "First puzzle: #{Day05.first_puzzle}"
  puts "Second puzzle: #{Day05.second_puzzle}"
end
