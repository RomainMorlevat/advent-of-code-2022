require "pry-byebug"

class Monkey
  attr_reader :divisible_by, :inspection_count, :items

  def initialize(attr: {})
    @divisible_by = attr.fetch(:divisible_by, 1)
    @formula = attr.fetch(:formula, "")
    @inspection_count = 0
    @items = attr.fetch(:items, [])
    @monkey_id_false = attr.fetch(:monkey_id_false)
    @monkey_id_true = attr.fetch(:monkey_id_true)
  end

  def inspect_item(item, divisor, lcm)
    return if @items.empty?

    @inspection_count += 1
    @items.shift
    (eval("#{item} #{@formula}") / divisor) % lcm
  end

  def receive_item(item)
    @items.push(item)
  end

  def throw_to(worry_level)
    (worry_level % @divisible_by).zero? ? @monkey_id_true : @monkey_id_false
  end
end

class Day11
  def initialize(input_file: "./data/day11-part01.txt", rounds: 20)
    @monkeys = populate_monkeys(input_file)
    @rounds = rounds
  end

  def first_puzzle
    run(3)
  end

  def second_puzzle
    # I had to look for others solutions to find how to not bruteforce this one 😞
    run(1)
  end

  private

  def run(divisor = nil)
    lcm = @monkeys.map(&:divisible_by).reduce(:lcm)
    @rounds.times do
      @monkeys.each do |monkey|
        items = monkey.items.dup
        items.each do |item|
          worry_level = monkey.inspect_item(item, divisor, lcm)
          throw_to_id = monkey.throw_to(worry_level)
          @monkeys[throw_to_id].receive_item(worry_level)
        end
      end
    end
    @monkeys.map(&:inspection_count).reject(&:zero?).max(2).reduce(:*)
  end

  def parse_input(input_file)
    monkeys = parse_monkeys(input_file)
    monkeys.map do |monkey|
      attributes = {}
      monkey.split("\n").each_with_index do |line, index|
        next if index.zero?

        case line
        when /Starting items/
          attributes[:items] = line.scan(/\d+/).map(&:to_i)
        when /Operation/
          attributes[:formula] = line.split("= old")[1].strip.gsub("old", "item")
        when /Test/
          attributes[:divisible_by] = line.scan(/\d+/)[0].to_i
        when /true/
          attributes[:monkey_id_true] = line.scan(/\d+/)[0].to_i
        when /false/
          attributes[:monkey_id_false] = line.scan(/\d+/)[0].to_i
        end
      end
      attributes
    end
  end

  def parse_monkeys(input_file)
    IO.read(input_file).split(/\n{2,}/)
  end

  def populate_monkeys(input_file)
    monkeys_attr = parse_input(input_file)
    monkeys_attr.map { |attr| Monkey.new(attr: attr) }
  end
end

unless ENV["TEST_ENV"]
  puts "First puzzle: #{Day11.new.first_puzzle}"
  puts "Second puzzle: #{Day11.new(rounds: 10000).second_puzzle}"
end
