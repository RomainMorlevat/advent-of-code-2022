require "pry-byebug"

class Day15
  class << self
    def compute_crossing_points(sensor, manhattan_distance, target_row)
      vertical_distance = (sensor[1] - target_row).abs
      return if vertical_distance > manhattan_distance

      horizontal_distance = manhattan_distance - vertical_distance
      [[sensor[0] - horizontal_distance, target_row], [sensor[0] + horizontal_distance, target_row]]
    end

    def compute_manhattan_distance(sensor, beacon)
      (sensor[0] - beacon[0]).abs + (sensor[1] - beacon[1]).abs
    end

    def exclude_beacon?(sensor, manhattan_distance, target_row)
      if sensor[1] > target_row
        sensor[1] - manhattan_distance > target_row
      else
        sensor[1] + manhattan_distance < target_row
      end
    end

    def first_puzzle(input_file = "./data/day15-part01.txt", target_row = 2_000_000)
      parsed_input = parse_input(input_file)
      sensors_and_manhattan = parsed_input.map do |sensor, beacon|
        [sensor, compute_manhattan_distance(sensor, beacon)]
      end
      filtered_sensors_and_manhattan = sensors_and_manhattan.reject do |sensor, manhattan_distance|
        exclude_beacon?(sensor, manhattan_distance, target_row)
      end
      diamonds_points_on_row = filtered_sensors_and_manhattan.map do |sensor, manhattan_distance|
        compute_crossing_points(sensor, manhattan_distance, target_row)
      end
      diamonds_points_on_row.flat_map do |coords|
        Range.new(coords[0][0], coords[1][0]).to_a
      end.uniq.count - 1
    end

    def merge_ranges(range_1, range_2)
      return nil if !ranges_overlap?(range_1, range_2) && !ranges_contiguous?(range_1, range_2)

      [range_1.begin, range_2.begin].min..[range_1.end, range_2.end].max
    end

    def ranges_contiguous?(range_1, range_2)
      range_1.end + 1 == range_2.begin
    end

    def ranges_overlap?(range_1, range_2)
      range_2.begin <= range_1.end && range_1.begin <= range_2.end
    end

    def second_puzzle(input_file = "./data/day15-part01.txt")
      parsed_input = parse_input(input_file)
      sensors_and_manhattan = parsed_input.map do |sensor, beacon|
        [sensor, compute_manhattan_distance(sensor, beacon)]
      end
      diamond_ranges = {}
      max = 4_000_000
      max.times do |x|
        sensors_and_manhattan.map do |sensor, manhattan_distance|
          compute_crossing_points(sensor, manhattan_distance, x)
        end.compact.each do |coords|
          if diamond_ranges.has_key?(coords[0][1])
            diamond_ranges[coords[0][1]] << Range.new(coords[0][0], coords[1][0])
          else
            diamond_ranges[coords[0][1]] = [Range.new(coords[0][0], coords[1][0])]
          end
        end
      end
      diamond_ranges.each_pair do |y, ranges|
        sorted_ranges = ranges.sort_by(&:begin)
        current_range = sorted_ranges[0]
        merged_ranges = []

        sorted_ranges.drop(1).each do |r|
          mr = merge_ranges(current_range, r)
          if mr.nil?
            merged_ranges << current_range
            current_range = r
          else
            current_range = mr
          end
        end
        merged_ranges << current_range
        diamond_ranges[y] = merged_ranges

        return ((merged_ranges[0].end + 1) * max) + y if merged_ranges.count > 1
      end
    end

    def parse_input(input_file = "./data/day15-part01.txt")
      IO.read(input_file).split("\n").map do |line|
        sx, sy, bx, by = line.scan(/-?\d+/).map(&:to_i)
        [[sx, sy], [bx, by]]
      end
    end
  end
end

unless ENV["TEST_ENV"]
  puts "First puzzle: #{Day15.first_puzzle}"
  start_time = Time.now
  puts "Second puzzle: #{Day15.second_puzzle}"
  puts "Second puzzle ran in #{Time.now - start_time}"
end
