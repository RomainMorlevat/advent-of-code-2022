class Day01
  def self.first_puzzle(input_file = "./data/day01-part01.txt")
    elves = IO.read(input_file).split(/\n{2,}/)
    sum_of_calories_for_each(elves).max
  end

  def self.second_puzzle(input_file = "./data/day01-part01.txt")
    elves = IO.read(input_file).split(/\n{2,}/)
    sum_of_calories_for_each(elves).max(3).sum
  end

  def self.sum_of_calories_for_each(elves)
    elves.map do |elf|
      elf.split.sum { |s| s.to_i }
    end
  end
end

unless ENV["TEST_ENV"]
  puts "First puzzle: #{Day01.first_puzzle}"
  puts "Second puzzle: #{Day01.second_puzzle}"
end
