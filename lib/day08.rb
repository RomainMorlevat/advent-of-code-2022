require "pry-byebug"

class Day08
  class << self
    def first_puzzle(input_file = "./data/day08-part01.txt")
      prepared_input = prepare_input(input_file)
      rotated_input = rotate_input(prepared_input)

      visible_trees = 0

      prepared_input.each_with_index do |row, row_index|
        row.each_with_index do |tree, tree_index|
          if row_index.zero? || row_index == prepared_input.length - 1 ||
              tree_index.zero? || tree_index == row.length - 1
            visible_trees += 1
          elsif tree > row[0...tree_index].max || tree > row[(tree_index + 1)..].max
            visible_trees += 1
          elsif tree > rotated_input[tree_index][0...row_index].max ||
              tree > rotated_input[tree_index][(row_index + 1)..].max
            visible_trees += 1
          end
        end
      end

      visible_trees
    end

    def second_puzzle(input_file = "./data/day08-part01.txt")
      prepared_input = prepare_input(input_file)
      rotated_input = rotate_input(prepared_input)

      best_scenic_score = 0

      prepared_input.each_with_index do |row, row_index|
        row.each_with_index do |tree, tree_index|
          next if row_index.zero? || tree_index == row.length - 1 ||
            row_index == row.length - 1 || tree_index.zero?

          index_trees_visible_up = rotated_input[tree_index][0...row_index].reverse.find_index { |t| t >= tree }
          trees_visible_up = index_trees_visible_up ? index_trees_visible_up + 1 : rotated_input[tree_index][0...row_index].length
          index_trees_visible_right = row[(tree_index + 1)..].find_index { |t| t >= tree }
          trees_visible_right = index_trees_visible_right ? index_trees_visible_right + 1 : row[(tree_index + 1)..].length
          index_trees_visible_down = rotated_input[tree_index][(row_index + 1)..].reverse.find_index { |t| t >= tree }
          trees_visible_down = index_trees_visible_down ? index_trees_visible_down + 1 : rotated_input[tree_index][(row_index + 1)..].length
          index_trees_visible_left = row[0...tree_index].reverse.find_index { |t| t >= tree }
          trees_visible_left = index_trees_visible_left ? index_trees_visible_left + 1 : row[0...tree_index].length

          scenic_score = trees_visible_up * trees_visible_right * trees_visible_down * trees_visible_left
          best_scenic_score = scenic_score if scenic_score > best_scenic_score
        end
      end

      best_scenic_score
    end

    def prepare_input(input_file)
      IO.read(input_file).split("\n").map { |n| n.chars.map(&:to_i) }
    end

    def rotate_input(prepared_input)
      prepared_input.transpose
    end
  end
end

unless ENV["TEST_ENV"]
  puts "First puzzle: #{Day08.first_puzzle}"
  puts "Second puzzle: #{Day08.second_puzzle}"
end
