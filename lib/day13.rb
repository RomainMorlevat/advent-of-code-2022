require "pry-byebug"

class Day13
  class << self
    def first_puzzle(input_file = "./data/day13-part01.txt")
      pairs = parse_input(input_file)
      pairs.map.with_index do |pair, index|
        ordered?(pair) ? index + 1 : 0
      end.reduce(:+)
    end

    def second_puzzle(input_file = "./data/day13-part01.txt")
      dividers = [[[2]], [[6]]]
      pairs = parse_input(input_file).flatten(1) + dividers
      sorted_pairs = pairs.sort { |a, b| compare([a, b]) }
      dividers.map do |divider|
        sorted_pairs.find_index(divider) + 1
      end.reduce(:*)
    end

    def ordered?(pair)
      compare(pair) != 1
    end

    def parse_input(input_file)
      IO.read(input_file)
        .split(/\n{2,}/)
        .map do |pair|
          pair.split("\n").map { |l| eval(l) }
        end
    end

    def compare(pair)
      left, right = pair
      if left.is_a?(Integer) && right.is_a?(Integer)
        left <=> right
      else
        left = [left] if left.is_a?(Integer)
        right = [right] if right.is_a?(Integer)

        min_length = [left.length, right.length].min
        (0...min_length).each do |i|
          c = compare([left[i], right[i]])
          return c if c.nonzero?
        end

        left.length <=> right.length
      end
    end
  end
end

unless ENV["TEST_ENV"]
  puts "First puzzle: #{Day13.first_puzzle}"
  puts "Second puzzle: #{Day13.second_puzzle}"
end
