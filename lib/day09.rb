require "pry-byebug"

class Day09
  MOVES = {
    "D" => [0, -1],
    "L" => [-1, 0],
    "R" => [1, 0],
    "U" => [0, 1]
  }.freeze

  def initialize(input_file = "./data/day09-part01.txt")
    @input = IO.read(input_file).split("\n").map { |line|
      dir, count = line.split(" ")
      [dir, count.to_i]
    }
    @map = create_map(500, 500)
    @h_coord = [50, 50]
    @t_coord = [50, 50]
  end

  def first_puzzle
    @input.each do |dir, count|
      count.times do
        @h_coord = move_head(dir)
        @t_coord = tail_follow
        @map[@t_coord[0]][@t_coord[1]] = "#"
      end
    end
    @map.flatten.compact.count
  end

  def second_puzzle
    knots_coord = [[50, 50]] * 10
    @input.each do |dir, count|
      count.times do
        knots_coord[0] = move_head(dir, knots_coord[0])
        knots_coord.each_with_index do |k, i|
          next if i.zero?

          knots_coord[i] = tail_follow(knots_coord[i - 1], k)
        end
        @map[knots_coord[-1][0]][knots_coord[-1][1]] = "#"
      end
    end
    @map.flatten.compact.count
  end

  def tail_follow(h_coord = @h_coord, t_coord = @t_coord)
    dist_x = h_coord[0] - t_coord[0]
    dist_y = h_coord[1] - t_coord[1]
    move_x = dist_x.zero? ? 0 : dist_x.abs / dist_x
    move_y = dist_y.zero? ? 0 : dist_y.abs / dist_y
    if dist_x.abs == 2 || dist_y.abs == 2
      [t_coord[0] + move_x, t_coord[1] + move_y]
    else
      t_coord
    end
  end

  def move_head(dir, h_coord = @h_coord)
    new_x = h_coord[0] + MOVES[dir][0]
    new_y = h_coord[1] + MOVES[dir][1]
    [new_x, new_y]
  end

  def create_map(width, height)
    Array.new(height) { Array.new(width) { nil } }
  end
end

unless ENV["TEST_ENV"]
  puts "First puzzle: #{Day09.new.first_puzzle}"
  puts "Second puzzle: #{Day09.new.second_puzzle}"
end
