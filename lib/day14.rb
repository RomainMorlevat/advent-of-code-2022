require "pry-byebug"

class Day14
  class << self
    def build_map(parsed_file, map_infos)
      x_max, x_min, y = map_infos
      map = Array.new(y + 1) { Array.new(x_max - x_min + 1) { "." } }
      parsed_file.each do |line|
        line.each_cons(2) do |c_start, c_end|
          if c_start[1] == c_end[1]
            range = (c_start[0] < c_end[0]) ? (c_start[0]..c_end[0]) : (c_end[0]..c_start[0])
            range.each do |c_x|
              map[c_start[1]][c_x - x_min] = "#"
            end
          else
            (c_start[1]..c_end[1]).each do |c_y|
              map[c_y][c_start[0] - x_min] = "#"
            end
          end
        end
      end
      map
    end

    def build_map_2(parsed_file)
      map = Array.new(500) { Array.new(700) { "." } }
      parsed_file.each do |line|
        line.each_with_index do |coords, idx|
          next if line[idx + 1].nil?

          x1 = coords[0]
          y1 = coords[1]
          x2 = line[idx + 1][0]
          y2 = line[idx + 1][1]
          if x1 == x2
            (y1, y2 = y2, y1) if y1 > y2
            (y1..y2).each do |y|
              map[y][x1] = "#"
            end
          end
          next unless y1 == y2

          (x1, x2 = x2, x1) if x1 > x2
          (x1..x2).each do |x|
            map[y1][x] = "#"
          end
        end
      end
      map
    end

    def fall(map, x, y)
      return [nil, nil] if x.zero? || x + 1 == map.first.length || map[y + 1].nil?

      if map[y + 1][x] == "."
        map[y][x] = "."
        map[y + 1][x] = "o"
        return fall(map, x, y + 1)
      elsif map[y + 1][x - 1] == "."
        map[y][x] = "."
        map[y + 1][x - 1] = "o"
        return fall(map, x - 1, y + 1)
      elsif map[y + 1][x + 1] == "."
        map[y][x] = "."
        map[y + 1][x + 1] = "o"
        return fall(map, x + 1, y + 1)
      end

      [x, y]
    end

    def first_puzzle(input_file = "./data/day14-part01.txt")
      parsed_file = parse_input(input_file)
      # map_infos = find_map_infos(parsed_file)
      # map = build_map(parsed_file, map_infos)
      map = build_map_2(parsed_file)
      units_of_sand = 0
      falling = true
      starting_point = 500 #- map_infos[1]
      while falling
        coord = fall(map, starting_point, 0)

        if coord == [nil, nil]
          falling = false
        end

        units_of_sand += 1
      end
      # map.each { |line| puts(line.join) }
      units_of_sand - 1
    end

    def second_puzzle(input_file = "./data/day14-part01.txt")
      parsed_file = parse_input(input_file)
      map = build_map_2(parsed_file)
      y_max = parsed_file.flatten(1).map { |x, y| y }.max
      map[y_max + 2] = Array.new(700) { "#" }
      units_of_sand = 0
      falling = true
      starting_point = 500
      while falling
        coord = fall(map, starting_point, 0)
        units_of_sand += 1
        if coord == [500, 0]
          falling = false
        end
      end
      # map.each { |line| puts(line.join) }
      units_of_sand
    end

    def find_map_infos(parsed_file)
      x_min, x_max = parsed_file.flat_map { |line| line.map { |x, _y| x } }.minmax
      y = parsed_file.flat_map { |line| line.map { |_x, y| y } }.max
      [x_max, x_min, y]
    end

    def parse_input(input_file = "./data/day14-part01.txt")
      IO.read(input_file).split("\n")
        .map do |line|
          line.split(" -> ").map do |coord|
            coord.split(",").map(&:to_i)
          end
        end.uniq
    end
  end
end

unless ENV["TEST_ENV"]
  puts "First puzzle: #{Day14.first_puzzle}"
  puts "Second puzzle: #{Day14.second_puzzle}"
end
