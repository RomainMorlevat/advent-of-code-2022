# Advent of code 2022

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/c4026d89fbb944839f593bb57dbd0dc7)](https://www.codacy.com/gl/RomainMorlevat/advent-of-code-2022/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=RomainMorlevat/advent-of-code-2022&amp;utm_campaign=Badge_Grade)

https://adventofcode.com/2022/

## How to run it

`ruby lib/dayXX.rb`

with XX the number of the day (01 to 25)
