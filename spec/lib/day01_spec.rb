require "day01"

RSpec.describe Day01 do
  describe "first_puzzle" do
    subject(:day01) { described_class.first_puzzle(input_file) }

    context "with one calories line" do
      let(:input_file) { "spec/fixtures/day01-part1-test01.txt" }

      it { is_expected.to eq(1234) }
    end

    context "with two calories line" do
      let(:input_file) { "spec/fixtures/day01-part1-test02.txt" }

      it { is_expected.to eq(6912) }
    end

    context "with Elves" do
      let(:input_file) { "spec/fixtures/day01-part1-test03.txt" }

      it { is_expected.to eq(9012) }
    end
  end

  describe "second_puzzle" do
    subject(:day01) { described_class.second_puzzle(input_file) }

    context "with four Elves" do
      let(:input_file) { "spec/fixtures/day01-part2-test01.txt" }

      it { is_expected.to eq(19) }
    end
  end
end
