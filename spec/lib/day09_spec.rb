require "day09"

RSpec.describe Day09 do
  subject(:day09) { described_class.new(input_file) }
  let(:input_file) { "spec/fixtures/day09-part1-test01.txt" }

  describe "#create_map(width, height)" do
    subject { day09.create_map(width, height) }

    context "with width 0 and height 0" do
      let(:width) { 0 }
      let(:height) { 0 }

      it { is_expected.to eq([]) }
    end

    context "with width 6 and height 5" do
      let(:width) { 6 }
      let(:height) { 5 }

      it {
        is_expected.to eq([
          [nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil]
        ])
      }
    end
  end

  describe "#first_puzzle" do
    subject { day09.first_puzzle }

    it { is_expected.to eq(13) }
  end

  describe "#move_head(dir)" do
    subject { day09.move_head(dir) }

    context "when R" do
      let(:dir) { "R" }

      it { is_expected.to eq([51, 50]) }
    end

    context "when L" do
      let(:dir) { "L" }

      it { is_expected.to eq([49, 50]) }
    end

    context "when U" do
      let(:dir) { "U" }

      it { is_expected.to eq([50, 51]) }
    end

    context "when D" do
      let(:dir) { "D" }

      it { is_expected.to eq([50, 49]) }
    end

    context "with head in [5, 3]" do
      before { day09.instance_variable_set(:@h_coord, [5, 3]) }

      context "when R" do
        let(:dir) { "R" }

        it { is_expected.to eq([6, 3]) }
      end

      context "when L" do
        let(:dir) { "L" }

        it { is_expected.to eq([4, 3]) }
      end

      context "when U" do
        let(:dir) { "U" }

        it { is_expected.to eq([5, 4]) }
      end

      context "when D" do
        let(:dir) { "D" }

        it { is_expected.to eq([5, 2]) }
      end
    end
  end

  describe "#second_puzzle" do
    subject { day09.second_puzzle }

    let(:input_file) { "spec/fixtures/day09-part2-test01.txt" }

    it { is_expected.to eq(36) }
  end

  describe "#tail_follow" do
    subject { day09.tail_follow }

    context "when head and tail at the same coordinates" do
      before do
        day09.instance_variable_set(:@h_coord, [0, 0])
        day09.instance_variable_set(:@t_coord, [0, 0])
      end

      it { is_expected.to eq([0, 0]) }
    end

    context "when head one move away" do
      before do
        day09.instance_variable_set(:@h_coord, [1, 0])
        day09.instance_variable_set(:@t_coord, [0, 0])
      end

      it { is_expected.to eq([0, 0]) }
    end

    context "when head two moves away" do
      before do
        day09.instance_variable_set(:@h_coord, [2, 0])
        day09.instance_variable_set(:@t_coord, [0, 0])
      end

      it { is_expected.to eq([1, 0]) }
    end

    context "when head one move in diagonal" do
      before do
        day09.instance_variable_set(:@h_coord, [1, 1])
        day09.instance_variable_set(:@t_coord, [0, 0])
      end

      it { is_expected.to eq([0, 0]) }
    end

    context "when head two moves in diagonal" do
      before do
        day09.instance_variable_set(:@h_coord, [2, 1])
        day09.instance_variable_set(:@t_coord, [0, 0])
      end

      it { is_expected.to eq([1, 1]) }
    end

    context "when head two moves in diagonal" do
      before do
        day09.instance_variable_set(:@h_coord, [1, 2])
        day09.instance_variable_set(:@t_coord, [0, 0])
      end

      it { is_expected.to eq([1, 1]) }
    end

    context "when head \"behind\" tail" do
      before do
        day09.instance_variable_set(:@h_coord, [0, 0])
        day09.instance_variable_set(:@t_coord, [2, 1])
      end

      it { is_expected.to eq([1, 0]) }
    end
  end
end
