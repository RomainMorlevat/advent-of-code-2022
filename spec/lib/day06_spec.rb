require "day06"

RSpec.describe Day06 do
  subject(:day06) { described_class.new(input_file) }

  describe "#first_puzzle" do
    subject { day06.first_puzzle }

    context "with first example" do
      let(:input_file) { "spec/fixtures/day06-part1-test01.txt" }

      it { is_expected.to eq(7) }
    end

    context "with second example" do
      let(:input_file) { "spec/fixtures/day06-part1-test02.txt" }

      it { is_expected.to eq(5) }
    end

    context "with third example" do
      let(:input_file) { "spec/fixtures/day06-part1-test03.txt" }

      it { is_expected.to eq(6) }
    end

    context "with fourth example" do
      let(:input_file) { "spec/fixtures/day06-part1-test04.txt" }

      it { is_expected.to eq(10) }
    end

    context "with fifth example" do
      let(:input_file) { "spec/fixtures/day06-part1-test05.txt" }

      it { is_expected.to eq(11) }
    end
  end

  describe "#second_puzzle" do
    subject { day06.second_puzzle }

    context "with first example" do
      let(:input_file) { "spec/fixtures/day06-part2-test01.txt" }

      it { is_expected.to eq(19) }
    end
  end
end
