require "day08"

RSpec.describe Day08 do
  subject(:day08) { described_class }

  let(:input_file) { "spec/fixtures/day08-part1-test01.txt" }
  let(:prepared_input) do
    [
      [3, 0, 3, 7, 3],
      [2, 5, 5, 1, 2],
      [6, 5, 3, 3, 2],
      [3, 3, 5, 4, 9],
      [3, 5, 3, 9, 0]
    ]
  end

  describe ".first_puzzle(input_file)" do
    subject { day08.first_puzzle(input_file) }

    context "with example" do
      it { is_expected.to eq(21) }
    end

    context "with six columns" do
      let(:input_file) { "spec/fixtures/day08-part1-test02.txt" }

      it { is_expected.to eq(25) }
    end
  end

  describe ".second_puzzle(input_file)" do
    subject { day08.second_puzzle(input_file) }

    context "with example" do
      it { is_expected.to eq(8) }
    end

    context "with six columns" do
      let(:input_file) { "spec/fixtures/day08-part1-test02.txt" }

      it { is_expected.to eq(24) }
    end
  end

  describe ".prepare_input(input_file)" do
    subject { day08.prepare_input(input_file) }

    context "with first example" do
      it { is_expected.to eq(prepared_input) }
    end
  end

  describe ".rotate_input(prepared_input)" do
    subject { day08.rotate_input(prepared_input) }

    let(:rotated_input) do
      [
        [3, 2, 6, 3, 3],
        [0, 5, 5, 3, 5],
        [3, 5, 3, 5, 3],
        [7, 1, 3, 4, 9],
        [3, 2, 2, 9, 0]
      ]
    end

    it { is_expected.to eq(rotated_input) }
  end
end
