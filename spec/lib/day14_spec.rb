require "day14"

RSpec.describe Day14 do
  subject(:day14) { described_class }

  let(:expected_map) do
    [
      [".", ".", ".", ".", ".", ".", ".", ".", ".", "."],
      [".", ".", ".", ".", ".", ".", ".", ".", ".", "."],
      [".", ".", ".", ".", ".", ".", ".", ".", ".", "."],
      [".", ".", ".", ".", ".", ".", ".", ".", ".", "."],
      [".", ".", ".", ".", "#", ".", ".", ".", "#", "#"],
      [".", ".", ".", ".", "#", ".", ".", ".", "#", "."],
      [".", ".", "#", "#", "#", ".", ".", ".", "#", "."],
      [".", ".", ".", ".", ".", ".", ".", ".", "#", "."],
      [".", ".", ".", ".", ".", ".", ".", ".", "#", "."],
      ["#", "#", "#", "#", "#", "#", "#", "#", "#", "."]
    ]
  end
  let(:input_file) { "spec/fixtures/day14-part1-test01.txt" }
  let(:parsed_file) do
    [
      [[498, 4], [498, 6], [496, 6]],
      [[503, 4], [502, 4], [502, 9], [494, 9]]
    ]
  end

  describe ".build_map(parsed_file, map_infos)" do
    subject { day14.build_map(parsed_file, [503, 494, 9]) }

    it { is_expected.to eq(expected_map) }
  end

  describe ".fall(map, x, y)" do
    subject(:fall) { day14.fall(map, x, y) }

    let(:map) { expected_map }

    context "when first step" do
      let(:x) { 6 }
      let(:y) { 0 }

      it "returns sand grain landing coordinates" do
        expect(fall).to eq([6, 8])
      end
    end

    context "when one sand grain on the ground" do
      let(:x) { 6 }
      let(:y) { 7 }

      before { map[8][6] = "o" }

      it "returns sand grain landing coordinates" do
        expect(fall).to eq([5, 8])
      end
    end

    context "when two sand grains on the ground" do
      let(:x) { 6 }
      let(:y) { 7 }

      before do
        map[8][5] = "o"
        map[8][6] = "o"
      end

      it "returns sand grain landing coordinates" do
        expect(fall).to eq([7, 8])
      end
    end

    context "when three sand grains on the ground" do
      let(:x) { 6 }
      let(:y) { 0 }

      before do
        map[8][5] = "o"
        map[8][6] = "o"
        map[8][7] = "o"
      end

      it "returns sand grain landing coordinates" do
        expect(fall).to eq([6, 7])
      end
    end

    context "when reaching the border of the map" do
      let(:x) { 0 }
      let(:y) { 8 }

      before do
        map[7][1] = "o"
        map[8][1] = "o"
      end

      it "returns sand grain landing coordinates" do
        expect(fall).to eq([nil, nil])
      end
    end

    context "when reaching the bottom of the map" do
      let(:x) { 6 }
      let(:y) { 9 }

      it "returns sand grain landing coordinates" do
        expect(fall).to eq([nil, nil])
      end
    end
  end

  describe ".first_puzzle" do
    subject { day14.first_puzzle(input_file) }

    it { is_expected.to eq(24) }
  end

  describe ".second_puzzle" do
    subject { day14.second_puzzle(input_file) }

    it { is_expected.to eq(93) }
  end

  describe ".find_map_infos(parsed_file)" do
    subject { day14.find_map_infos(parsed_file) }

    it { is_expected.to eq([503, 494, 9]) }
  end

  describe ".parse_input(input_file)" do
    subject { day14.parse_input(input_file) }

    it { is_expected.to eq(parsed_file) }
  end
end
