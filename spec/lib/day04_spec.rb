require "day04"

RSpec.describe Day04 do
  describe ".first_puzzle" do
    subject { described_class.first_puzzle(input_file) }

    context "when duplicated work" do
      let(:input_file) { "spec/fixtures/day04-part1-test01.txt" }

      it { is_expected.to eq(2) }
    end
  end

  describe ".second_puzzle" do
    subject { described_class.second_puzzle(input_file) }

    context "when overlaping work" do
      let(:input_file) { "spec/fixtures/day04-part1-test01.txt" }

      it { is_expected.to eq(4) }
    end
  end
end
