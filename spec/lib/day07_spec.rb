require "day07"

RSpec.describe Day07 do
  subject(:day07) { described_class }

  describe ".build_tree(input_file)" do
    subject { day07.build_tree(input_file) }

    context "with one directory" do
      let(:input_file) { "spec/fixtures/day07-part1-test01.txt" }

      it { is_expected.to eq([23352670]) }
    end

    context "with two nested directories" do
      let(:input_file) { "spec/fixtures/day07-part1-test02.txt" }

      it { is_expected.to eq([23446939, [94269]]) }
    end

    context "with full example" do
      let(:input_file) { "spec/fixtures/day07-part1-test03.txt" }

      it { is_expected.to eq([48381165, [94853, [584]], [24933642]]) }
    end
  end

  describe ".first_puzzle(tree)" do
    subject { day07.first_puzzle(tree) }

    context "with full example" do
      let(:tree) { [48381165, [94853, [584]], [24933642]] }

      it { is_expected.to eq(95437) }
    end
  end

  describe ".second_puzzle(tree)" do
    subject { day07.second_puzzle(tree) }

    context "with full example" do
      let(:tree) { [48381165, [94853, [584]], [24933642]] }

      it { is_expected.to eq(24933642) }
    end

    context "with closest upper value to target" do
      let(:tree) { [48381165, [94853, [584]], [24933642], [30000001]] }

      it { is_expected.to eq(24933642) }
    end

    context "with two possible values" do
      let(:tree) { [48381165, [94853, [584]], [24933642], [8381166]] }

      it { is_expected.to eq(8381166) }
    end
  end
end
