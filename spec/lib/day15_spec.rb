require "day15"

RSpec.describe Day15 do
  subject(:day15) { described_class }

  let(:input_file) { "spec/fixtures/day15-part1-test01.txt" }
  let(:parsed_file) do
    [
      [[2, 18], [-2, 15]],
      [[9, 16], [10, 16]],
      [[13, 2], [15, 3]],
      [[12, 14], [10, 16]],
      [[10, 20], [10, 16]],
      [[14, 17], [10, 16]],
      [[8, 7], [2, 10]],
      [[2, 0], [2, 10]],
      [[0, 11], [2, 10]],
      [[20, 14], [25, 17]],
      [[17, 20], [21, 22]],
      [[16, 7], [15, 3]],
      [[14, 3], [15, 3]],
      [[20, 1], [15, 3]]
    ]
  end

  describe ".compute_crossing_points(sensor, manhattan_distance, target_row)" do
    subject { day15.compute_crossing_points(sensor, manhattan_distance, target_row) }

    let(:sensor) { [8, 7] }
    let(:manhattan_distance) { 9 }

    context "when diamond of the sensor cross target_row" do
      let(:target_row) { 10 }

      it { is_expected.to eq([[2, 10], [14, 10]]) }
    end

    context "when diamond of the sensor cross target_row" do
      let(:target_row) { 20 }

      it { is_expected.to eq(nil) }
    end
  end

  describe ".compute_manhattan_distance(sensor, beacon)" do
    subject { day15.compute_manhattan_distance(sensor, beacon) }

    let(:sensor) { [2, 18] }
    let(:beacon) { [-2, 15] }

    it { is_expected.to eq(7) }
  end

  describe ".exclude_beacon?(sensor, manhattan_distance, target_row)" do
    subject { day15.exclude_beacon?(sensor, manhattan_distance, 10) }

    context "when diamond cross target_row" do
      let(:sensor) { [8, 7] }
      let(:manhattan_distance) { 9 }

      it { is_expected.to eq(false) }
    end

    context "when diamond does not cross target_row" do
      let(:sensor) { [2, 18] }
      let(:manhattan_distance) { 7 }

      it { is_expected.to eq(true) }
    end

    context "when diamond does not cross target_row" do
      let(:sensor) { [13, 2] }
      let(:manhattan_distance) { 3 }

      it { is_expected.to eq(true) }
    end
  end

  describe ".first_puzzle(input_file, target_row)" do
    subject { day15.first_puzzle(input_file, 10) }

    it { is_expected.to eq(26) }
  end

  describe ".merge_ranges(range_1, range_2)" do
    subject { day15.merge_ranges(range_1, range_2) }

    let(:range_1) { (1..5) }

    context "when ranges overlap" do
      let(:range_2) { (4..6) }

      it { is_expected.to eq((1..6)) }
    end

    context "when ranges does not overlap" do
      let(:range_2) { (7..8) }

      it { is_expected.to be_nil }
    end

    context "when ranges are contiguous" do
      let(:range_2) { (6..7) }

      it { is_expected.to eq((1..7)) }
    end
  end

  describe ".second_puzzle(input_file)" do
    subject { day15.second_puzzle(input_file) }

    it { is_expected.to eq(56000011) }
  end

  describe ".parse_input(input_file)" do
    subject { day15.parse_input(input_file) }

    it { is_expected.to eq(parsed_file) }
  end

  describe ".ranges_overlap?(range_1, range_2)" do
    subject { day15.ranges_overlap?(range_1, range_2) }

    let(:range_1) { (1..5) }

    context "when ranges overlap" do
      let(:range_2) { (4..6) }

      it { is_expected.to eq(true) }
    end

    context "when ranges does not overlap" do
      let(:range_2) { (6..7) }

      it { is_expected.to eq(false) }
    end
  end

  describe ".ranges_contiguous?(range_1, range_2)" do
    subject { day15.ranges_contiguous?(range_1, range_2) }

    let(:range_1) { (1..5) }

    context "when ranges overlap" do
      let(:range_2) { (6..7) }

      it { is_expected.to eq(true) }
    end

    context "when ranges does not overlap" do
      let(:range_2) { (7..8) }

      it { is_expected.to eq(false) }
    end
  end
end
