require "day11"

RSpec.describe Monkey do
  subject(:monkey) do
    described_class.new(attr: {
      items: items,
      formula: formula,
      divisible_by: divisible_by,
      monkey_id_true: monkey_id_true,
      monkey_id_false: monkey_id_false
    })
  end
  let(:items) { [79, 60] }
  let(:formula) { "*item" }
  let(:divisible_by) { 13 }
  let(:monkey_id_false) { 3 }
  let(:monkey_id_true) { 1 }

  context "when inspect items" do
    before { monkey.inspect_item(79, 3, 1) }

    context "with items" do
      it "count number of inspections" do
        expect(monkey.inspection_count).to eq(1)
      end

      it "unload the item" do
        expect(monkey.instance_variable_get(:@items)).to eq([60])
      end
    end

    context "with no items" do
      let(:items) { [] }

      it "do not increment number of inspections" do
        expect(monkey.inspection_count).to eq(0)
      end
    end
  end

  describe "#inspect_item(item, divisor)" do
    subject(:inspect_item) { monkey.inspect_item(item, 3, 3002) }
    # 3002 is Least Common Multiplicator for 79 and 38

    context "when monkey inspects an item with a worry level of 79" do
      let(:item) { 79 }

      it { is_expected.to eq(2080) }
    end

    context "when monkey inspects an item with a worry level of 38 and formula +6" do
      let(:item) { 38 }
      let(:formula) { "+6" }

      it { is_expected.to eq(14) }
    end
  end

  describe "#receive_item(item)" do
    subject(:receive_item) { monkey.receive_item(666) }

    it "add the received item to @items" do
      receive_item
      expect(monkey.instance_variable_get(:@items)).to eq([79, 60, 666])
    end
  end

  describe "#throw_to" do
    subject { monkey.throw_to(worry_level) }

    context "when worry_level divisible by 13" do
      let(:worry_level) { 2080 }

      it { is_expected.to eq(1) }
    end

    context "when worry_level not divisible by 13" do
      let(:worry_level) { 1200 }

      it { is_expected.to eq(3) }
    end
  end
end

RSpec.describe Day11 do
  subject(:day11) { described_class.new(input_file: input_file, rounds: rounds) }

  let(:input_file) { "spec/fixtures/day11-part1-test01.txt" }
  let(:rounds) { 1 }

  context "with example" do
    it "has 5 monkeys" do
      expect(day11.instance_variable_get(:@monkeys).count).to eq(4)
    end

    it "first monkey as formula * 19" do
      expect(day11.instance_variable_get(:@monkeys).first.instance_variable_get(:@formula)).to eq("* 19")
    end
  end

  describe "#first_puzzle" do
    subject(:first_puzzle) { day11.first_puzzle }

    context "with 20 rounds" do
      let(:rounds) { 20 }

      it { is_expected.to eq(10_605) }
    end
  end

  describe "#second_puzzle" do
    subject(:second_puzzle) { day11.second_puzzle }

    context "with 10_000 rounds" do
      let(:rounds) { 10_000 }

      it { is_expected.to eq(2_713_310_158) }
    end
  end
end
