require "day03"

RSpec.describe Day03 do
  describe ".first_puzzle" do
    subject { described_class.first_puzzle(input_file) }

    context "when one rucksack" do
      let(:input_file) { "spec/fixtures/day03-part1-test01.txt" }

      it { is_expected.to eq(16) }
    end

    context "with example" do
      let(:input_file) { "spec/fixtures/day03-part1-test02.txt" }

      it { is_expected.to eq(157) }
    end
  end

  describe ".second_puzzle" do
    subject { described_class.second_puzzle(input_file) }

    context "when one group of three elves" do
      let(:input_file) { "spec/fixtures/day03-part2-test01.txt" }

      it { is_expected.to eq(18) }
    end

    context "when one group of three elves" do
      let(:input_file) { "spec/fixtures/day03-part1-test02.txt" }

      it { is_expected.to eq(70) }
    end
  end
end
