require "day05"

RSpec.describe Day05 do
  let(:input_file) { "spec/fixtures/day05-part1-test01.txt" }

  describe ".build_columns(columns)" do
    subject { described_class.build_columns(columns) }

    context "with three columns" do
      let(:columns) { ["    [D]", "[N] [C]", "[Z] [M] [P]", " 1   2   3"] }

      it { is_expected.to eq({"1" => ["Z", "N"], "2" => ["M", "C", "D"], "3" => ["P"]}) }
    end
  end

  describe ".build_moves(moves)" do
    subject { described_class.build_moves(moves) }

    context "with four moves" do
      let(:moves) { ["move 1 from 2 to 1", "move 3 from 1 to 3", "move 2 from 2 to 1", "move 1 from 1 to 2"] }

      it { is_expected.to eq([["1", "2", "1"], ["3", "1", "3"], ["2", "2", "1"], ["1", "1", "2"]]) }
    end
  end

  describe ".first_puzzle" do
    subject { described_class.first_puzzle(input_file) }

    context "with three columns and four moves" do
      let(:input_file) { "spec/fixtures/day05-part1-test01.txt" }

      it { is_expected.to eq("CMZ") }
    end
  end

  describe ".second_puzzle" do
    subject { described_class.second_puzzle(input_file) }

    context "with three columns and four moves" do
      let(:input_file) { "spec/fixtures/day05-part1-test01.txt" }

      it { is_expected.to eq("MCD") }
    end
  end

  describe ".split_input(input)" do
    subject { described_class.split_input(input) }

    let(:input) { IO.read(input_file).split("\n") }

    it {
      is_expected.to eq([
        ["    [D]", "[N] [C]", "[Z] [M] [P]", " 1   2   3"],
        ["move 1 from 2 to 1", "move 3 from 1 to 3", "move 2 from 2 to 1", "move 1 from 1 to 2"]
      ])
    }
  end
end
