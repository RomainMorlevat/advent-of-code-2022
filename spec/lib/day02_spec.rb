require "day02"

RSpec.describe Day02 do
  describe ".first_puzzle" do
    subject { described_class.first_puzzle(input_file) }

    context "when one round" do
      let(:input_file) { "spec/fixtures/day02-part1-test01.txt" }

      it { is_expected.to eq(8) }
    end

    context "when three round" do
      let(:input_file) { "spec/fixtures/day02-part1-test02.txt" }

      it { is_expected.to eq(15) }
    end

    context "when six round" do
      let(:input_file) { "spec/fixtures/day02-part1-test03.txt" }

      it { is_expected.to eq(28) }
    end
  end

  describe ".second_puzzle" do
    subject { described_class.second_puzzle(input_file) }

    context "when one round" do
      let(:input_file) { "spec/fixtures/day02-part1-test01.txt" }

      it { is_expected.to eq(4) }
    end

    context "when three round" do
      let(:input_file) { "spec/fixtures/day02-part1-test02.txt" }

      it { is_expected.to eq(12) }
    end
  end
end
