require "day13"

RSpec.describe Day13 do
  subject(:day13) { described_class }

  describe ".first_puzzle" do
    subject { day13.first_puzzle(input_file) }

    let(:input_file) { "spec/fixtures/day13-part1-test02.txt" }

    it { is_expected.to eq(13) }
  end

  describe ".second_puzzle" do
    subject { day13.second_puzzle(input_file) }

    let(:input_file) { "spec/fixtures/day13-part1-test02.txt" }

    it { is_expected.to eq(140) }
  end

  describe ".ordered?(pair)" do
    subject { day13.ordered?(pair) }

    context "when Integer" do
      context "with 1 vs 2" do
        let(:pair) { [1, 2] }

        it { is_expected.to eq(true) }
      end

      context "with 2 vs 1" do
        let(:pair) { [2, 1] }

        it { is_expected.to eq(false) }
      end
    end

    context "when Array" do
      context "with [1,1,3,1,1] vs [1,1,5,1,1]" do
        let(:pair) { [[1, 1, 3, 1, 1], [1, 1, 5, 1, 1]] }

        it { is_expected.to eq(true) }
      end

      context "with [7,7,7,7] vs [7,7,7]" do
        let(:pair) { [[7, 7, 7, 7], [7, 7, 7]] }

        it { is_expected.to eq(false) }
      end

      context "with 9 vs [8,7,6]" do
        let(:pair) { [9, [8, 7, 6]] }

        it { is_expected.to eq(false) }
      end

      context "with [[9], [[8, 7, 6]]]" do
        let(:pair) { [[9], [[8, 7, 6]]] }

        it { is_expected.to eq(false) }
      end

      context "with [8,7,6] vs 9" do
        let(:pair) { [[8, 7, 6], 9] }

        it { is_expected.to eq(true) }
      end

      context "with [] vs [3]" do
        let(:pair) { [[], [3]] }

        it { is_expected.to eq(true) }
      end

      context "with [[[]]] vs [[]]" do
        let(:pair) { [[[[]]], [[]]] }

        it { is_expected.to eq(false) }
      end

      context "with [1,[2,[3,[4,[5,6,7]]]],8,9] vs [1,[2,[3,[4,[5,6,0]]]],8,9]" do
        let(:pair) { [[1, [2, [3, [4, [5, 6, 7]]]], 8, 9], [1, [2, [3, [4, [5, 6, 0]]]], 8, 9]] }

        it { is_expected.to eq(false) }
      end

      context "with [[1],[2,3,4]] vs [[1],4]" do
        let(:pair) { [[[1], [2, 3, 4]], [[1], 4]] }

        it { is_expected.to eq(true) }
      end
    end
  end

  describe ".parse_input(input_file)" do
    subject { day13.parse_input(input_file) }

    let(:input_file) { "spec/fixtures/day13-part1-test01.txt" }
    let(:expected_result) do
      [
        [[9], [[8, 7, 6]]],
        [[], [3]]
      ]
    end

    it { is_expected.to eq(expected_result) }
  end
end
