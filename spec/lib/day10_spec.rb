require "day10"

RSpec.describe Day10 do
  subject(:day10) { described_class }

  describe "#first_puzzle" do
    subject { day10.first_puzzle(input_file) }

    context "with 220 cycles example" do
      let(:input_file) { "spec/fixtures/day10-part1-test04.txt" }

      it { is_expected.to eq(13140) }
    end
  end

  describe "#second_puzzle" do
    subject { day10.second_puzzle(input_file) }

    context "with 220 cycles example" do
      let(:input_file) { "spec/fixtures/day10-part1-test04.txt" }
      let(:expected_display) do
        ["##..##..##..##..##..##..##..##..##..##..",
          "###...###...###...###...###...###...###.",
          "####....####....####....####....####....",
          "#####.....#####.....#####.....#####.....",
          "######......######......######......####",
          "#######.......#######.......#######....."]
      end

      it { is_expected.to eq(expected_display) }
    end
  end
end
